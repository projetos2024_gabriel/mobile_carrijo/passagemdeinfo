import React from "react";
import { View, Button, Text, TouchableOpacity, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";

const TaskList = ({ navigation }) => {

    const tasks = [
        {
            id: 1,
            title: 'Santos maior do mundo',
            date: '2024-02-27',
            time: '10:00',
            address: 'Santos',
        },
        {
            id: 2,
            title: 'Jogar bola pro Santos',
            date: '2024-02-28',
            time: '14:00',
            address: 'Vila Belmiro',
        },
        {
            id: 3,
            title: 'Charles do bronks',
            date: '2024-02-29',
            time: '08:30',
            address: 'Academia Fitness',
        },
    ];
const taskPress=(task)=>{
    navigation.navigate('DetalhesDasTarefas',{task})
}
    return (
        <View>
            <Text>Lista de Usuários</Text>
            <FlatList
                data={tasks}
                keyExtractor={(item) => item.id.toString()} //<Text>Idade:{item.age}</Text>
                renderItem={({ item }) => (
                    <TouchableOpacity onPress={() => taskPress(item)} >
                        <Text>Titulo:{item.title}</Text>
                    </TouchableOpacity>
                )}
            />
        </View>
    );
};
export default TaskList;



